﻿using System;

namespace MatrixDemo;
public static class Program
{
    public static void Main(string[] args)
    {
        double[,] d = new double[2, 2]
        {
            { 1.0, 2.0 },
            { 3.0, 4.0 },
        };
        Matrix m = new Matrix(d);
        Console.WriteLine(m.ToString());
    }
}